import json
import math
import sys
import uuid
from graphics import *

class Cup:

  
  def __init__(self,base,height,top,name=None,shape="cylinder",verbose=False):
    self.base = base
    self.contents = 0
    self.content_percent = 0
    self.content_height = 0
    self.height = height
    self.shape = shape
    self.spill_last = 0
    self.spill_total = 0
    self.top  = top

    if name:
      self.name = name
    else:
      self.name = str(uuid.uuid4())

    if self.shape == "cylinder":
      r = math.pow(self.base,2) + self.base * self.top + math.pow(self.top,2)
      self.volume = 1/3 * math.pi * r * self.height
      self.slant = math.sqrt((math.pow(self.base - self.top,2) + math.pow(self.height,2)))
      self.lateral_surface = (self.top + self.base) * math.pi * self.slant

      #r = math.pow(self.content_base,2) + self.content_base * self.content_top + math.pow(self.content_top,2)
      
      a = self.height
      if self.top > self.base:
        b = (self.top - self.base) / 2
        self.theta = a/b
      elif self.base > self.top:
        b = (self.base - self.top) / 2
        self.theta = a/b
      else:
        b = 0
        self.theta = 0
      
      if verbose == True:
        print("v = 1/3 * " + str(math.pi) + " * ( " + str(self.base) + "² + " + str(self.base) + " * " + str(self.top) + " + " + str(self.top) + "² ) * " + str(self.height))

    if verbose == True:
      print("A new cup has been created:")
      print("  Base Radius: " + str(self.base))
      print("  Top Radius: " + str(self.top))
      print("  Height: " + str(self.height))
      print("  Volume: " + str(self.volume))
      print("  Slant: " + str(self.slant))
      print("")


  def Refresh(self,verbose=False,maths=True):
    self.Check_Spill(verbose=verbose)
    self.content_percent = self.contents / self.volume * 100
    
    self.content_height  = self.content_percent / 100 * self.height
    
    self.content_base  = self.base
  
    # a = math.pi / 3
    # a = a * self.content_height * self.content_base

    # b = math.pow(a,2)

    # c = math.pi / 3
    # c = c * math.pow(self.content_base,2) * self.content_height
    # c = c - self.contents
    # c = 4 * math.pi / 3 * self.content_height * c

    # d = 2 * math.pi /3 * self.content_height

    # self.content_top = - a + math.sqrt(b-c)
    # self.content_top = self.content_top / d

    # if maths == True:
    #   print("Maths breakdown:")
    #   print("  a = pi/3 * h * R")
    #   print("  a = " + str(math.pi / 3) + " * " + str(self.content_height) + " * " + str(self.content_base))
    #   print("  a = " + str(a))
    #   print("")
    #   print("  b = (pi/3 * h * R) ^ 2")
    #   print("  b = a ^ 2")
    #   print("  b = " + str(b))
    #   print("")
    #   print("  c = 4 * pi/3 * h * (pi/3 * R^2 * h - v)")
    #   print("  c = 4 * " + str(math.pi / 3) + " * " + str(self.content_height) + " * (" +  str(math.pi / 3) + " * " + str(math.pow(self.content_base,2)) + " * " + str(self.content_height) + " - " + str(self.contents) + ")")
    #   print("  c = " + str(c))
    #   print("")
    #   print("  d = 2 * pi/3 * h")
    #   print("  d = 2 * " + str(math.pi / 3) + " * " + str(self.content_height))
    #   print("  d = " + str(d))
    #   print("")
    #   print("  r = -a + sqrt(b-c) / d")
    #   print("  r = - " + str(a) + " + sqrt(" + str(b-c) + ") / " + str(d))
    #   print("  r = - " + str(a) + " + " + str(math.sqrt(b-c)) + " / " + str(d))
    #   print("  r = " + str(-a+math.sqrt(b-c)) + " / " + str(d) )
    #   print("  r = " + str(self.content_top))
    #   print("")

    R = self.base
    r = self.top

    a = R - r
    a = a / self.height

    self.content_top = R - a * self.height
    #self.content_top = 150

    if verbose == True:
      print("Cup contents updated:")
      print("  Content volume: " + str(self.contents))
      print("  Content %: " + str(self.content_percent))
      print("  Content height: " + str(self.content_height))
      print("  Content base radius: " + str(self.content_base))
      print("  Content top radius: " + str(self.content_top))
      print("")


  def Fill(self,amount=None,verbose=False):
    if not amount:
      amount = self.volume - self.contents
    self.contents = self.contents + amount

    if verbose == True:
      print("Filling the cup with " + str(self.contents))

    self.Refresh(verbose=verbose)


  def Drain(self,amount=None,verbose=False):
    if amount:
      self.contents = self.contents - amount
    
    if verbose == True:
      print("Draining " + str(amount) + " from the cup")
    
    self.Refresh(verbose=verbose)


  def Check_Spill(self,extra_amount=None,verbose=False):
    self.spill_last = 0
    if self.contents > self.volume:
      self.spill_last = self.contents - self.volume
      self.contents = self.volume
      
      if verbose == True:
        print("The last operation has caused the cup to overflow:")
        print("  Last spillage: " + str(self.spill_last))

    if extra_amount:
      self.spill_last = self.spill_last + extra_amount
      self.contents = self.contents - extra_amount

      if verbose == True:
        print("The last operation has caused the cup to spill:")
        print("  Last spillage: " + str(self.spill_last))

    self.spill_total = self.spill_total + self.spill_last

    if self.spill_last != 0:
      if verbose == True:
        print("  Total spillage: " + str(self.spill_total))
        print("")
      
      self.Refresh(verbose=verbose)


  def show(self,format="human",verbose=False):
    if format == "json":
      data = {
        self.name: {
          "properties": {
            "shape": self.shape,
            "base radius": self.base,
            "top radius": self.top,
            "height": self.height,
            "volume": self.volume,
            "slant": self.slant,
          },
          "contents": {
            "amount": self.contents,
            "percent": self.content_percent,
            "height": self.content_height,
          },
        }
      }
      print(json.dumps(data,indent=4,sort_keys=True))
    elif format == "human":
      print("Cup properties:")
      print("  Name: " + self.name)
      print("  Shape: " + self.shape)
      print("  R (top radius)  = " + str(self.top))
      print("  r (base radius) = " + str(self.base))
      print("  H (Height)      = " + str(self.height))
      print("  V (Volume)      = " + str(self.volume))
      print("  S (Slant)       = " + str(self.slant))
      print("  Lateral Surface = " + str(self.lateral_surface))
      print("Cup contents:")
      print("  v (Volume)      = " + str(self.contents))
      print("  % (% of Total)  = " + str(self.content_percent))
      print("  h (height)      = ?????") # + str(self.content_height))
      print("  y (top radius)  = ?????") # + str(self.content_top))
      print("  z (base radius) = " + str(self.content_base))
      print("")
    elif format == "draw":
      window_width  = 500
      window_height = 500
      margin_bottom = window_height - round(10/100 * window_height)

      top_height = margin_bottom - self.height
      top_left   = window_width - self.top
      top_left   = top_left / 2
      top_right  = top_left+self.top

      bottom_height = margin_bottom      
      bottom_left   = window_width - self.base
      bottom_left   = bottom_left / 2
      bottom_right  = bottom_left + self.base

      content_height = margin_bottom - self.content_height
      content_bottom_left = bottom_left
      content_bottom_right = bottom_right
      content_top_left = window_width - self.content_top
      content_top_left = content_top_left / 2
      content_top_right = content_top_left + self.content_top

      if verbose == True:
        print("Drawing")
        print("  Window Width: " + str(window_width))
        print("  Window Height: " + str(window_height))
        print("  Bottom Margin: " + str(margin_bottom))
        print("  Top:")
        print("    Height: " + str(top_height))
        print("    Left Margin: " + str(top_left))
        print("    Size: " + str(self.top))
        print("  Base:")
        print("    Height: " + str(bottom_height))
        print("    Left Margin: " + str(bottom_left))
        print("    Size: " + str(self.base))
        

      win = GraphWin(self.name, window_width, window_height) # give title and dimensions

      cup = Polygon(
        Point(bottom_left,bottom_height), Point(bottom_right,bottom_height),
        Point(top_right,top_height), Point(top_left,top_height),
      )
      cup.setWidth(2)
      cup.draw(win)

      contents = Polygon(
        Point(content_bottom_left,bottom_height), Point(content_bottom_right,bottom_height),
        Point(content_top_right,content_height), Point(content_top_left,content_height),
      )
      contents.setWidth(3)
      contents.setFill("blue")
      contents.draw(win)

      message = Text(Point(win.getWidth()/2, 20), 'Click anywhere to quit.')
      message.draw(win)
      win.getMouse()
      win.close()



def main():
  top=80
  base=80
  height=90
  fill=880000
  drain=0

  glass = Cup(top=top,base=base,height=height,verbose=True)
  glass.Fill(fill,verbose=True)
  #glass.Drain(drain,verbose=True)
  glass.show()
  glass.show(format="draw",verbose=True)

if sys.version_info[0] != 3:
  print("This requires python 3")
  sys.exit(1)
if __name__ == "__main__":
  main()
